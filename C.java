/* The following Java program demonstrates how to combine the state of two objects
   without using Comparator */

import java.lang.reflect.Field;/* importing the package to provides information about,
								and dynamic access to, a single field of a class or an interface.*/
import java.lang.Class;//importing this package as it provides several utility methods

public class Demo  //Creating a class Demo which contains compare method
{
	// Declaring static method so that this method will be accessible to all the classes
	public static boolean compare(Object o,Object p)throws IllegalAccessException //this method can throw IllegalAccessException
	{

		boolean flag = false;// creating the flag variable with return type as boolean
		try
		{
			if(0 == null || p == null)// if object is null then it will return false
			{
				return false;
			}
			//comparing the classes of two objects declared 
			if(o.getClass().equals(p.getClass()))
			{

				Field[]  field = o.getClass().getDeclaredFields();//getting the object referring class fields
				for(Field f:field)//Using for for each loop for traversing
				{
					//setting the accessibility to be true 
					f.setAccessible(true);//Setting the accessibility true so it can access fields
					if(f.get(o).equals(f.get(p)))//comparing the fields of both object
					{

						flag = true;// then assigning true to the flag which is boolean data type

					}
					else
					{
						flag = false;// if not equals we are assigning false value to the flag
						break;
					}	

				}
			}
			else if(o.getClass().getSuperclass().equals(p.getClass()))// comparing if the object is of superclass with object referring class
			{
				System.out.println("Inside super class method");
	
				Field[]  fi = o.getClass().getDeclaredFields();//Creating Field[] imported from reflection class
				System.out.println("Inside super class method");

				for(Field f:fi)// To Traverse we used for each loop
					{
						System.out.println("Inside super class method");
						f.setAccessible(true);//setting the accessibility true so we can access methods
						if(f.get(o).equals(f.get(p)))//it fetches the value in the object and compares it
							{
								System.out.println("Inside super class method");

								flag = true;

							}
						else
						{		
							flag = false;
							break;
						}

						
					}
			}
			else if(o.getClass()!= p.getClass()  )//checking if classes are not equal
			{
				flag = false;

			}
			else
			{
				System.out.println("Do Nothing");
			}
		}
		
		catch(NullPointerException EB)//trying to catch Null pointer Exception
		{
			System.out.println("You have created a NullPointerException");
			System.out.println("Your object referring to null");
		}
		catch(IllegalArgumentException EA)//trying to catch Illegal argument Exception
		{
			System.out.println("You have created an IllegalArgumentException");
			System.out.println("Your method have passed an illegal arguments ");
		}
		catch(IllegalAccessException EA)//trying to catch Illegal access Exception
		{
			System.out.println("You have created an IllegalAccessException");
			System.out.println("Your object cannot access the class methods ");
		}
		catch(Exception EC)//trying to catch some undefined Exception
		{
			System.out.println("you have encountered an Exception");
		}
		if(flag)//if flag value is true it will print its block of code
		{

			System.out.println("Same state");
		}
		else//else it will print this will print the following block
		{
			System.out.println("Not in the Same state");
		}	
		return flag;// returning the value of flag

	}	
}
 

----------------------------------------------------------------------------------------------------------------------------
 
 /* class A used for comparing its state object*/
 
 public class A
{
	int a,b;
	A()//created a zero argument constructor
	{


	}
	A(int a,int b)//created two argument constructor by overloading it
	{

		this.a = a;
		this.b = b;

	}	


}


/* class B used for comparing its state object*/
 
 class B 
{
	int a,b;
	B(int a,int b)// two argument constructor is created
	{

		this.a = a;
		this.b = b;

	}	


}

---------------------------------------------------------------------------------------------------------------------------------

/* class Base used for comparing objects which are used to check
  probability of inheritance */
  
  public class Base //creating a base class for checking if the objects inherited classes 
{
	int a,b;
	Base()//constructor is created which is no argument constructor
	{


	}
	Base(int a,int b)//two argument constructor is created
	{
		this.a = a;
		this.b= b;

	}

}

------------------------------------------------------------------------------------------------------------------------

/* class Child used to inherit the base 
and through which we can compare the code */

class Child extends Base //this class is extending its parent class Base
{

	int a,b,c,d;
	Child(int a,int b,int c,int d)// constructor is created
	{

	super(a,b);//super keyword to call the methods or constructor in its base class or super class
	this.c = c;
	this.d = d;

	}

}

------------------------------------------------------------------------------------------------------------------------------------

/* class launcher identified as C is used for creating objects of every class we have 
created and then also calls the compare method we coded in Demo class*/

public class C //creating a launcher class (which has main method)
{
	public static void main(String args[])throws Exception//this main method may throw any exception
	{

		A obj = new A(10,20);// creating object for class A
		A obj1 = new A(10,20);//creating object for class A again
		B obj2 = new B(10,20);// creating object for class B 
		B obj3 = new B(10,20);//creating object for class B again
		Base obj4 = new Base(10,20);//creating object for class base
		Child obj5 = new Child(10,20,30,40);// creating object class Child
		

		Demo.compare(obj,obj1);
	}

}